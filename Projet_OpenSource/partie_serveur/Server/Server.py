import socket
import threading
import tkinter
from tkinter import ttk # cette importation me permettra de créer la liste pour le classement
from PIL import Image
import random
import pickle

class ClientThread(threading.Thread): # cette classe est un thread

    list_of_client = [] # liste qui va contenir tous les socket qui ont été entendue par le serveur

    def __init__(self, ip, port, clientsocket):
        threading.Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.clientsocket = clientsocket

    def run(self): # cette fonction est appelé lorsqu'un client a réussi à ce connecter
   
        print("Connexion de %s %s" % (self.ip, self.port, ))

        # lorsqu'un client est connecté, on lui envoie le background du jeux et un chiffre entre 0 et 2 compris
        ListOfImage = ["./Img/mountain.jpg" , "./Img/space1.jpeg" , "./Img/space2.jpeg" , "./Img/space3.jpeg", "./Img/space4.jpeg", "./Img/space5.jpeg"] # liste des chemin vers les image en background
        randomIndex = random.choice(range(len(ListOfImage))) # je choisie un chiffre random
        image_background = open(ListOfImage[randomIndex], "rb") # je récupére l'image
        imageBinaryBytes = image_background.read() # j'encode l'image en byte
        # partie player
        image_player = open('./Img/fusee.png','rb')
        image_player_binary_byte = image_player.read()
        # partie météorite
        image_meteorite = open('./Img/meteorite.png','rb')
        image_mateorite_binary_byte = image_meteorite.read()

        dicoToSend = {
            "backgroundImage" : imageBinaryBytes,
            "player_image" : image_player_binary_byte,
            "meteorite_image" : image_mateorite_binary_byte,
            "difficulty" : random.randrange(0,2)
        }
        byte_to_send = pickle.dumps(dicoToSend) # je sérialise le dictionnaire
        self.clientsocket.send(byte_to_send) # je lui envoie le dictionnaire sérialisé (en byte)

        # et à la fin, j'ajoute ce client dans la liste des client
        self.list_of_client.append(self.clientsocket)

        while True:
            self.data_received = self.clientsocket.recv(1024)
            a_classe = int.from_bytes(self.data_received, 'big')
            print(a_classe)
            if a_classe == 0: # si un envoie de 0 est recu alors on arréte la réception
                break
            else:
            # première idée => ensuite je vais appeler une méthode statique qui va mettre à jour le classement 
                SetTheMenu(self.port,a_classe)
                # t1 = SetTheMenu(self.port,a_classe)
                # t1.start()


class Server:
    tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    IsListening = False
    def __init__(self):
        self.tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # ici on crée un socket selon la famille AF_INET et la constante SOCK_STREAM
        self.tcpsock.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1) # ici on initialise les option du socket (pour mon serveur je veux que les client reste connecté afin de recevoir des infos de leur part)
        self.tcpsock.bind(("127.0.0.1",1234))
        self.IsListening = True

        #SetTheMenu(0,0) # ca va crée une fenêtre tkinter avec le classement, meilleur score etc

        while True:
            if self.tcpsock.listen: # si le serveur est en train d'écouté alors il peut prendre un port et accepté des client
                self.tcpsock.listen(10)
                print( "En écoute...")
                (clientsocket, (ip, port)) = self.tcpsock.accept() # la méthode accept est une méthode (synchrone) qui va permettre d'accepter les client
                newthread = ClientThread(ip, port, clientsocket)
                newthread.start()
            else: # sinon on sort de la boucle
                break

class SetTheMenu():
    data = []
    def __init__(self,port,time):
        self.port = port
        self.time = time
        self.run()

    def run(self,):
        self.root = tkinter.Tk() # on créer la frame 
        self.root.title("The Menu")
        self.root.geometry("720x500")
        self.SetAllWidgets(self.port,self.time) # j'appel cette méthode pour qu'il met en place les widgets

        # je personnalise la fenêtre en disant que tant que la fenêtre est ouvert, le serveur est en train d'écouté
        self.root.protocol("WM_DELETE_WINDOW", self.window_will_be_closed)

        self.root.mainloop()        

    def window_will_be_closed(self):
        Server.tcpsock.close() # 


    def SetAllWidgets(self,port,time):

        # Partie Treeview
        columns = ('id','score')
        self.treeview1 = ttk.Treeview(self.root , columns=columns, show='headings')
        self.treeview1.heading('id' , text='ID')
        self.treeview1.heading('score',text='Score')
        self.treeview1.place(relx=0.5, rely=0.5, anchor=tkinter.CENTER)

        # Partie ScrollBar
        scrollbar = ttk.Scrollbar(self.root, orient=tkinter.VERTICAL, command=self.treeview1.yview)
        self.treeview1.configure(yscrollcommand=scrollbar.set)

        info = [port,int(time/1000)]
        self.data.append(info)
        for item in self.data:
            self.treeview1.insert('', tkinter.END, values=item)
   #     info = [port,time]
    #    SetTheMenu.treeview1.insert('', tkinter.END, values=info)



Server()