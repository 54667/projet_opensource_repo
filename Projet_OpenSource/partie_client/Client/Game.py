import pygame as pyg
import sys
from io import BytesIO
from PIL import Image
import time
import random
import threading
from pygame.locals import *
from pygame import mixer
import os

class SetTheGame:
    def __init__(self , dico_imgs_difficulty, socket_of_client):
        self.socket_client = socket_of_client
        self.dico_imgs_difficulty = dico_imgs_difficulty        

        pyg.init() # j'initialise la fenêtre de pygame
        pyg.display.set_caption("Jeu Test") # je met un titre à la fenêtre
        # je cache le curseur
        pyg.mouse.set_visible(False)
        self.SetBackground()

        #region partie image du player et vitesse
        # je met en place la vitesse et un array qui me permettra d'aller en heut ou en bas
        self.rocket = SetRocket(self.dico_imgs_difficulty)
        #endregion

        #region partie meteorite
        self.meteorite = SetMeteor(self.dico_imgs_difficulty)
        # je clear les météorite qui était la avant
        self.meteorite.array_image_meteorite.clear()
        self.meteorite.array_rect_meteorite.clear()
        self.meteorite.array_location_y.clear()
        #endregion
        # Je crée un objet qui permet de pisté le temps (me permet d'utiliser plusieurs fonction lié au temps)
        self.clock = pyg.time.Clock()

        self.timer_to_send_to_server = pyg.time.get_ticks() # j'instancie le timer qui va être envoyé au serveur
        self.timer_started_to = pyg.time.get_ticks() # représente le début du timer

        self.number_of_meteor_put_since_last_event = 0
        self.time_to_spawn = 5 / self.meteorite.number_of_meteorite_to_spawn # Ceci correpond au temps dans lequel la météorite peut spawn (c'est 5 sec / par le nombre de meteorite qu'on doit spawn)
        
        self.intial_delay = 1 # c'est l'intervalle par défaut pour que les météorite spawn

        # partie musique de fond
        mixer.init()
        test = os.getcwd()
        mixer.music.load(test +'./musics/tintin_musique.wav')
        mixer.music.play()     

        while True:
            # au début je configure le timer, => j'envoie le timer à la classe setmeteor
            self.current_time = pyg.time.get_ticks()
            if (self.current_time - self.timer_started_to)/1000 > 5: # si on a atteint 5 secondes alors on remet le timer à 0
                self.timer_started_to = pyg.time.get_ticks() # c'est ici que je réinitialise le point start 
                self.meteorite.number_of_meteorite_to_spawn += 1 # ici j'incrémente de 1 le nombre de météorite à spawn

            self.SetTime() # met en place l'objet clock
            for event in pyg.event.get(): # pour chaque event que pygame percoit alors
                if event.type == pyg.QUIT: 
                    pyg.quit()
                    sys.exit() # si l'event correspond a pyg.QUIT alors le systéme peut s'arréter
            # Je met en place le rafraichissement des images avec la méthode blit et je termine avec flip pour actualiser la frame
            self.screen.blit(self.background_image,(0,0))
            self.game_in_loop()
            pyg.display.update()
            # pyg.display.flip()
    def SetBackground(self):
        byte_image_background = BytesIO(self.dico_imgs_difficulty["backgroundImage"] ) # me permet de désérialisé le buffer qui représente l'image
        image_background = Image.open(byte_image_background , "r") # je créer l'instance qui représente l'image
        # # j'initialise la taille de la fenêtre 
        self.screen = pyg.display.set_mode( (image_background.width,image_background.height) ) # J'initialise la taille de la fenêtre
        # # ensuite j'initialise l'image en background
        sizeOfBackground = image_background.size
        mode = image_background.mode
        # self.background_image = pyg.image.load(image_background)
        # self.background_image = pyg.image.frombuffer(bytes= self.dico_imgs_difficulty["backgroundImage"] , size = sizeOfBackground ,format='RGB' ).convert()
        self.background_image = pyg.image.fromstring(image_background.tobytes(),sizeOfBackground,mode).convert()    # type: ignore
        byte_image_background.close()# je ferme ensuite le stream

    def set_all_controlor(self):
        # je met en place les touches
        # key = pyg.key.get_pressed()
        # if key[pyg.K_RIGHT]: # le joueur va à droite
        #     self.velocity[0] = 1
        # elif key[pyg.K_LEFT]: # le joueur va à gauche
        #     self.velocity[0] = -1
        # else: # sinon on remet à 0 sa vélocité sur l'axe x
        #     self.velocity[0] = 0

        # if key[pyg.K_UP]: # le joueur va en haut
        #     self.velocity[1] = -1
        # elif key[pyg.K_DOWN]: # le joueur en bas
        #     self.velocity[1] = 1
        # else:
        #     self.velocity[1]=0
        # ou je fait avec la souris
        self.mx,self.my = pyg.mouse.get_pos() # => ici je récupére les coordonné de la souris
        # ensuite je vais vérifier si ces valeurs ne sorte pas de la fenêtre, si il sort d'en bas ou d'en alors il réapparait de l'autre coté
        if self.mx + self.rocket.rect_player.width > self.screen.get_rect().width:
            self.mx -= self.screen.get_rect().width
        elif self.my + self.rocket.rect_player.height > self.screen.get_rect().height:
            self.my -= self.screen.get_rect().height
                 
    def game_in_loop(self):
        self.set_all_controlor() # ceci va mettre en place les controleur et va permetre le déplacement

        # j'appel la méthode self.move_the_player qui va être à chaque fois appelé car il fait partie de la game_in_loop
        #self.move_the_player() # => j'appel cette méthode si je veut jouer avec des touches

        # je vais ensuite faire appel à la méthode qui permet de faire spawn le nombre de meteorite
        self.spawn_all_meteor()
        #j'appel la méthode qui va faire apparaitre des météorites 
        self.verify_Collider()

        # j'actualise la hitbox du player
        self.rocket.rect_player = self.rocket.image_player.get_rect(x=self.mx , y=self.my)
        # partie rafraichissement => ATTENTION à l'odre d'utilisation de la méthode blit, le premier blit est mis le plus bas (donc je commence par le background)
        self.screen.blit(self.rocket.image_player,(self.mx,self.my))

    def verify_Collider(self):
        # self.rect_meteorite.bottom += 4 # -> pour faire de la gravité
        # je vérifie s'il y a eu collision avec toute les méteorite
        for rect_meteorite in self.meteorite.array_rect_meteorite:
            if self.rocket.rect_player.colliderect(rect_meteorite):
                # j'envoie le temps au serveur
                time_to_send_in_byte = self.current_time - self.timer_to_send_to_server
                byte_to_send = time_to_send_in_byte.to_bytes(10 , byteorder='big')
                self.socket_client.send(byte_to_send)
                pyg.quit()
                sys.exit()
                
                
    def SetTime(self):
        self.clock.tick(120) # la méthode tick permettra de mettre à jour le self.clock => elle me permettra principalement à gérer les vitesse de déplacement

    def spawn_all_meteor(self): # c'est ici que les meteorite vont être crée et mis en place
        # for i in range(self.meteorite.number_of_meteorite_to_spawn): # de 0 au nombre de meteore à mettre
        #     self.meteorite.create_meteorite() # il va juste créer les images
        # tant que le nombre de météorite n'ont pa été TOUTES mis alors,
        #  

        # rappel self.time_to_spawn est l'intervalle de temps pour qu'une meteorite spawn MAIS qui atteint 10 sec
        # alors que self.intervalle est la vraie intervalle
        timer_for_spawn_meteor = (self.current_time - self.timer_started_to)/1000 # cette variable sera toujours entre 0 et 10(représente le temps)
        
        if timer_for_spawn_meteor >= self.time_to_spawn - 0.06: 
            self.number_of_meteor_put_since_last_event += 1 # on va créer une météorite donc on incrémente le nombre de météorite qui ont été mis
            self.time_to_spawn += self.intial_delay
            # print(self.intial_delay)
            self.PutTheNewMeteorite()
        # et si toute les météorite ont été mise alors on va changer le self.time_to_spawn pour que les météorite spawn de plus en plus vite
        if self.number_of_meteor_put_since_last_event == self.meteorite.number_of_meteorite_to_spawn:
            self.time_to_spawn = 5 / (self.meteorite.number_of_meteorite_to_spawn + 1) # c'est le nouveau timer
            self.intial_delay = self.time_to_spawn
            self.number_of_meteor_put_since_last_event = 0
        self.Actualise_Meteorite() # j'actualise les météorite

    def PutTheNewMeteorite(self):
        self.meteorite.create_meteorite() # va créer l'image, la hitbox
        # je prend un chiffre aléatoire entre 0 et self.background_image.get_width() => ce chiffre sera mis dans le tableau self.meteorite.array_of_locationy
        random_location = random.randint(0,self.background_image.get_width())
        self.meteorite.array_location_y.append(random_location) # c'est ici que je rajoute la localisation de la météorite
    
    def Actualise_Meteorite(self):
        # ensuite je vais initialisé les position des meteorite (et l'actualisé) si il en existe
        if len(self.meteorite.array_image_meteorite) > 0:
            for image_meteorite in self.meteorite.array_image_meteorite:
                #je calcule la position du météore aprés qu'il tombe de 3 pixels
                index_of_image = self.meteorite.array_image_meteorite.index(image_meteorite) # je récuprére l'index de l'image
                self.meteorite.array_rect_meteorite[index_of_image].y += 1
                location_meteorite_y = self.meteorite.array_rect_meteorite[index_of_image].y
                # print(f"la localisation en x {self.meteorite.array_location_y[index_of_image]} et y est {location_meteorite_y}")
                # je vais supprimer les météorite qui sont en dehors de la frame
                if self.is_in_frame(location_meteorite_y , index_of_image): # s'il est dans la frame alors fait le blit
                    # et j'actualise l'image
                    self.screen.blit(image_meteorite,(self.meteorite.array_location_y[index_of_image],location_meteorite_y))
                    # j'actualise la hitbox de toute les meteorite
                    self.meteorite.array_rect_meteorite[index_of_image] = image_meteorite.get_rect(x= self.meteorite.array_location_y[index_of_image] ,y=location_meteorite_y)
                else: # sinon, on ne fait rien
                    pass
   
    def is_in_frame(self , location_meteorite_y , index_of_image): # ceci va supprimer les météorite en dehors de la frame
        if location_meteorite_y > self.background_image.get_height(): # si la localisation de la météorite est plus grand que la taille du background alors return false
            return False
        else:
            return True
        
    # def move_the_player(self):
    #     # je met en place le déplacement de la hitbox
    #     self.rect_player.move_ip(self.velocity[0] * self.speed,self.velocity[1] * self.speed) # la vitesse de déplacment est 10


class SetMeteor:
    dicoOfInfo = {}
    number_of_meteorite_to_spawn = 5
    array_image_meteorite = []
    array_rect_meteorite = []
    array_location_y = [] # array qui va stocker la localisation de meteorite sur l'axe y

    def __init__(self, dicoOfInfo): # le constructeur ne va que initialisé le dictionnaire
        self.dicoOfInfo = dicoOfInfo

    def create_meteorite(self):
        self.byte_of_meteorite_image = BytesIO(self.dicoOfInfo["meteorite_image"] ) 
        self.image_meteorite = Image.open(self.byte_of_meteorite_image)
        # je charge l'image du player
        self.image_meteorite = pyg.image.fromstring(self.image_meteorite.tobytes(),self.image_meteorite.size,self.image_meteorite.mode).convert_alpha()   # type: ignore
        self.array_image_meteorite.append(self.image_meteorite)
        # je met en place les hitbox (deux choix, je crée un objet rect ou je crée une hitbox mais juste de cette image)
        rect_meteorite = self.image_meteorite.get_rect(x=0 , y=0) # 0,0 correpond à l'emplacement initiale de la box
        self.array_rect_meteorite.append(rect_meteorite)
        self.byte_of_meteorite_image.close()

class SetRocket:
    speed = 5
    velocity = [0,0]
    def __init__(self , dico_serv):
        byte_of_rocket_image = BytesIO(dico_serv["player_image"] ) 
        image_rocket = Image.open(byte_of_rocket_image)
        # je charge l'image du player
        self.image_player = pyg.image.fromstring(image_rocket.tobytes(),image_rocket.size,image_rocket.mode).convert_alpha()  # type: ignore
        # je met en place les hitbox (deux choix, je crée un objet rect ou je crée une hitbox mais juste de cette image)
        self.rect_player = self.image_player.get_rect(x=0 , y=0) # 0,0 correpond à l'emplacement initiale de la box
        byte_of_rocket_image.close()