import tkinter
import pygame as pyg
import sys
import socket
import pickle
from PIL import Image
from io import BytesIO
import threading
import Game as game


class SetTheGame(threading.Thread):
    def __init__(self , dicoOfInformation, socket_of_client):
        threading.Thread.__init__(self) # ici on dit que cette classe est un thread donc il faut la méthode run
        self.dicoOfInformation = dicoOfInformation
        self.socket_client = socket_of_client

    def run(self):
        game.SetTheGame(self.dicoOfInformation,self.socket_client)


class Client:
    def __init__(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect(("127.0.0.1", 1234)) # il se connecte

        all_bytes = b''
        yo = 1
        r = b''
        while True:
            print(yo)
            try:
                DataReceived = pickle.loads(all_bytes)
                break
            except:
                r = s.recv(4096)
            all_bytes += r
            yo += 1

        DataReceived = pickle.loads(all_bytes) # il désérialise la donnée qu'il vient de recevoir 

        # et la je lance le jeu
        t1 = SetTheGame(DataReceived,s)
        t1.start()

class SetTheMenu(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self) # ici on dit que cette classe est un thread donc il faut la méthode run

    def run(self): # c'est ici que je vais utiliser tkinter pour faire un menu
        self.root = tkinter.Tk() # on créer la frame 
        self.root.title("The Menu")
        self.root.geometry("720x500")
        self.SetAllWidgets() # j'appel cette méthode pour qu'il met en place les widgets
        self.root.mainloop()

    def SetAllWidgets(self):
        self.panel = tkinter.PanedWindow(self.root , bg="black")
        self.panel.pack(fill='both', expand=1)
        # bouton qui va commencer le jeu
        self.buttonOfServer = tkinter.Button(self.panel, text="Play" , command=lambda : self.Start_game())
        self.buttonOfServer.place(x=25, y=100)

    def Start_game(self):
        Client()
        
t1 = SetTheMenu()
t1.start()